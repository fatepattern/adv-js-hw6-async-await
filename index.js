let button = document.querySelector(".clickme");

function showData(data){
  let root = document.getElementById('root')
  let text = document.createElement('text');
  text.innerText = `Your location: ${data.country}, ${data.regionName}, ${data.city}, ${data.zip}`
  root.appendChild(text);
}

async function getDataFromIp(ip){
  try{
    const response = await fetch(`http://ip-api.com/json/${ip}`);
    const data = await response.json();
    return data;
  } catch(err){
    console.log(err);
  }
}

button.addEventListener("click", async function(){
  try{
    const response = await fetch('https://api.ipify.org/');
    const ip = await response.text();
    console.log(ip);
    const dataFromIp = await getDataFromIp(ip);
    showData(dataFromIp);
    button.style.display = "none";
  } catch (err) {
    console.log(err);
  }
});